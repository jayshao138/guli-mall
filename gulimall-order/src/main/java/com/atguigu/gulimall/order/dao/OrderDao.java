package com.atguigu.gulimall.order.dao;

import com.atguigu.gulimall.order.entity.OrderEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * ????
 * 
 * @author jayerListen
 * @email jayshao138@163.com
 * @date 2023-07-31 14:58:07
 */
@Mapper
public interface OrderDao extends BaseMapper<OrderEntity> {
	
}
