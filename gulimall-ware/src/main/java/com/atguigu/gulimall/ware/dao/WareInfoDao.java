package com.atguigu.gulimall.ware.dao;

import com.atguigu.gulimall.ware.entity.WareInfoEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 仓库信息
 * 
 * @author jayerListen
 * @email jayshao138@163.com
 * @date 2023-07-31 15:05:47
 */
@Mapper
public interface WareInfoDao extends BaseMapper<WareInfoEntity> {
	
}
