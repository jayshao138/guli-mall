package com.atguigu.gulimall.product.entity;

import com.atguigu.common.valid.AddGroup;
import com.atguigu.common.valid.ListValue;
import com.atguigu.common.valid.UpdateGroup;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;
import lombok.NonNull;
import org.hibernate.validator.constraints.URL;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.*;

/**
 * Ʒ?
 *
 * @author jayerListen
 * @email jayshao138@163.com
 * @date 2023-07-30 05:57:49
 */
@Data
@TableName("pms_brand")
public class BrandEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * Ʒ??id
	 */
	@NotNull(message = "修改必须指定id", groups = { UpdateGroup.class})
	@Null(message = "新增不能指定id", groups = {AddGroup.class})
	@TableId
	private Long brandId;
	/**
	 * Ʒ????
	 */
	@NotBlank (message = "品牌名不能为空！", groups = {AddGroup.class, UpdateGroup.class})
	private String name;
	/**
	 * Ʒ??logo??ַ
	 */
	@NotEmpty(message = "新增时不能为空", groups = {AddGroup.class})
	@URL(message = "logo必须是一个合法的url地址", groups = {AddGroup.class, UpdateGroup.class})
	private String logo;
	/**
	 * ???
	 */
	private String descript;
	/**
	 * ??ʾ״̬[0-????ʾ??1-??ʾ]
	 */

//	@Min(value = 0, message = "值必须为0或1", groups = {AddGroup.class, UpdateGroup.class})
//	@Max(value = 1, message = "值必须为0或1", groups = {AddGroup.class, UpdateGroup.class})
	@NotNull(message = "新增时不能为空", groups = {AddGroup.class})
	@ListValue(vals={0,1}, groups = {AddGroup.class, UpdateGroup.class})
	private Integer showStatus;
	/**
	 * ????????ĸ
	 */
	@NotEmpty(message = "新增时不能为空", groups = {AddGroup.class})
	@Pattern(regexp = "[a-zA-Z]",message = "检索首字母必须是一个字母",groups = {AddGroup.class, UpdateGroup.class})
	private String firstLetter;
	/**
	 * ???
	 */
	@NotNull(message = "新增时不能为空", groups = {AddGroup.class})
	@Min(value = 0, message = "值必须大于0",groups = {AddGroup.class, UpdateGroup.class})
	private Integer sort;

}
