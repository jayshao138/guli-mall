package com.atguigu.gulimall.product.dao;

import com.atguigu.gulimall.product.entity.BrandEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * Ʒ?
 * 
 * @author jayerListen
 * @email jayshao138@163.com
 * @date 2023-07-30 05:57:49
 */
@Mapper
public interface BrandDao extends BaseMapper<BrandEntity> {
	
}
